**********
Push nodes
**********

A common issue of building up a comp script is the lack of space between nodes
to add more nodes. The ``push_nodes.push`` function offers the functionality to
push all nodes around a selection in specified directions.


Rules
=====

The ``push`` function follow these rules to maintain the order of the script as
best as possible.

- All selected nodes and Backdrop nodes stay where they are and only nodes
  in the specified direction are moved.
- Selected backdrops should not be move nor re-sized.
- Deselected backdrops surrounding selected nodes should be re-sized.
- Overlapping backdrops and their content should not be moved when pushing
  in one direction only.
- Nodes snapped to the grid should stay snapped after being moved (can be
  disabled for accurate offsets).


Pivot
=====

If no node is selected the last click position in the DAG will be used.

.. image:: ./img/push_nodes_pivot.gif
    :width: 350pt


Examples
========

Push nodes down
---------------

.. code-block:: python

    from nuke_move_nodes.push_nodes import push

    push(down=True)

.. image:: ./img/push_nodes_down.gif
    :width: 350pt

Push nodes up
-------------

Similarly pushing nodes up:

.. code-block:: python

    push(up=True)

.. image:: ./img/push_nodes_up.gif
    :width: 350pt

Push nodes left
---------------

.. code-block:: python

    push(left=True)

.. image:: ./img/push_nodes_left.gif
    :width: 350pt

Up and down
-----------

.. code-block:: python

    push(up=True, down=True)

.. image:: ./img/push_nodes_up_down.gif
    :width: 350pt

All directions
--------------

.. code-block:: python

    push(up=True, down=True, left=True, right=True)

.. image:: ./img/push_nodes_all_directions.gif
    :width: 350pt


``menu.py`` integration
=======================

The menu integration obviously depends on your setup. A simple
``menu.py`` integration could look as following:


.. code-block:: python

    push_menu = nuke.menu('Nuke').addMenu('Push')

    push_menu.addCommand(
        'Down',
        'from nuke_move_nodes import push_nodes; push_nodes.push(down=True)',
         'ctrl+PgDown',
         shortcutContext=2
    )
    push_menu.addCommand(
        'Up',
        'from nuke_move_nodes import push_nodes; push_nodes.push(up=True)',
         'ctrl+PgUp',
         shortcutContext=2
    )
    push_menu.addCommand(
        'Left',
        'from nuke_move_nodes import push_nodes; push_nodes.push(left=True)',
    )
    push_menu.addCommand(
        'Right',
        'from nuke_move_nodes import push_nodes; push_nodes.push(right=True)',
    )

