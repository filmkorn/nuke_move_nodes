[![Build](https://gitlab.com/filmkorn/nuke_move_nodes/badges/master/build.svg)](https://gitlab.com/filmkorn/nuke_move_nodes/pipelines)

# nuke_move_nodes

Move nodes in the DAG.

[Documentation](https://filmkorn.gitlab.io/nuke_move_nodes/)
