Changelog
=========

1.0.2 (June 25th, 2019)
-----------------------
  - Add documentation (#5)
  - Add changelog (#4)

1.0.1 (June 3rd, 2019)
----------------------
  - Fix failing tests (#3)
  - Clean up doc strings (#2)

1.0.0 (June 2nd, 2019)
----------------------
  - Initial release (#1)

    - All selected nodes stay where they are and only nodes in the specified direction are moved.
    - If no node is selected, create a node as pivot to get last clicked position in DAG.
    - Selected backdrops should not be re-sized.
    - Deselected backdrops surrounding selected nodes should be re-sized.
    - Overlapping backdrops and their content should not be moved if pushing in one direction only.
