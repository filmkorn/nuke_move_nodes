"""Set up the package."""
from setuptools import setup, find_packages

setup(
    name="nuke_move_nodes",
    packages=find_packages(),
    install_requires=[],
    tests_require=['parameterized'],
    setup_requires=['setuptools_scm'],
    python_requires='>=2.7,<4',
    package_data={
        # Include the test .nk script.
        '': ['data/*.nk'],
    },
    author="Mitja Mueller-Jend",
    author_email="mitja.muellerjend@gmail.com",
    description="Move nodes in the DAG.",
    license="MIT",
    keywords="nuke move push nodes DAG resize backdrops grid size snapped",
    url="https://gitlab.com/filmkorn/nuke_move_nodes",
    project_urls={
        "Bug Tracker": "https://gitlab.com/filmkorn/nuke_move_nodes/issues",
        "Source Code": "https://gitlab.com/filmkorn/nuke_move_nodes/",
    },
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    use_scm_version = True,
)
